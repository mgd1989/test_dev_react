import React from 'react';
import './RegisterForm.css';
import withFormSubscription from "./withFormSubscription";
import {Button} from "react-bootstrap";
import Input from "../SharedComponents/Input";

function LoginForm({ values, handleChange, handleSubmit, errors }) {
  return (
      <div className="customForm">
        <form onSubmit={handleSubmit}>
          <Input id='login_email' name='email' type='email' placeholder='Email' value={values.email} label='Email' onChange={(e) => handleChange(e, 'email')} error={errors.email} required={true} />
          <Input id='login_password' name='password' type='Password' placeholder='Password' value={values.password} label='Password' onChange={(e) => handleChange(e, 'password')} error={errors.password} required={true} />
          <Button variant="primary" type="submit">Login</Button>
          <Button variant="link">Mot de passe oublié ?</Button>
        </form>
      </div>
  );
}

export default withFormSubscription(LoginForm, {email: '', password: ''});
