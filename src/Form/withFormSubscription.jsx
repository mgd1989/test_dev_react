import React from 'react';
import useForm from "../Hooks/useForm";

function withFormSubscription(WrappedComponent, formFields) {
    return function FormWrapper() {
        const formLogin = () => {
            alert(JSON.stringify(values));
        }

        const {handleChange, values, errors, handleSubmit} = useForm(formLogin, formFields);
        const formProps = {
            values,
            handleChange,
            handleSubmit,
            errors,
        };

        return <WrappedComponent {...formProps} />;
    };
}

export default withFormSubscription;
