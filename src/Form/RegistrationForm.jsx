import React from 'react';
import './RegisterForm.css';
import withFormSubscription from "./withFormSubscription";
import {Button} from "react-bootstrap";
import Input from "../SharedComponents/Input";

function RegistrationForm({ values, handleChange, handleSubmit, errors }) {
  return (
      <div className="customForm">
        <form onSubmit={handleSubmit}>
            <Input id='register_email' name='email' type='email' placeholder='Email' value={values.email} label='Email' onChange={(e) => handleChange(e, 'email')} error={errors.email} required={true} />
            <Input id='register_password' name='password' type='Password' placeholder='Password' value={values.password} label='Password' onChange={(e) => handleChange(e, 'password')} error={errors.password} required={true} />
            <Input id='register_confirm_password' name='confirm' type='Password' placeholder='Confirmation password' value={values.confirm} label='Confirm Password' onChange={(e) => handleChange(e, 'confirm')} error={errors.confirm} required={true} />

          <Button variant="primary" type="submit">Register</Button>
        </form>
      </div>
  );
}

export default withFormSubscription(RegistrationForm, {email: '', password: '', confirm: ''});
