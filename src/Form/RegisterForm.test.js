import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import RegistrationForm from "./RegistrationForm";

describe('RegisterForm Component', () => {
    it('renders without errors', () => {
        const { getByLabelText } = render(<RegistrationForm />);
        const loginEmailInput = getByLabelText('Email');
        expect(loginEmailInput).toBeInTheDocument();
    });

    it('handles form submission correctly', () => {
        window.alert = jest.fn();
        const { getByText, getByLabelText } = render(<RegistrationForm />);
        const emailInput = getByLabelText('Email');
        const passwordInput = getByLabelText('Password');
        const confirmInput = getByLabelText('Confirm Password');
        const submitButton = getByText('Register');

        fireEvent.change(emailInput, { target: { value: 'example@example.com' } });
        fireEvent.change(passwordInput, { target: { value: '12345' } });
        fireEvent.change(confirmInput, { target: { value: '12345' } });
        fireEvent.click(submitButton);
        window.alert.mockClear();
    });

    it('handles form submission with error', () => {
        window.alert = jest.fn();
        const { getByText, getByLabelText } = render(<RegistrationForm />);
        const emailInput = getByLabelText('Email');
        const passwordInput = getByLabelText('Password');
        const confirmInput = getByLabelText('Confirm Password');
        const submitButton = getByText('Register');

        fireEvent.change(emailInput, { target: { value: 'test' } });
        fireEvent.change(passwordInput, { target: { value: '12345' } });
        fireEvent.change(confirmInput, { target: { value: '12345' } });
        fireEvent.click(submitButton);
        window.alert.mockClear();
        const emailErrorText = getByText('You have to provide a valid email.');
        expect(emailErrorText).toBeInTheDocument();
    });
});





