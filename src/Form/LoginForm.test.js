import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import LoginForm from './LoginForm';

describe('LoginForm Component', () => {
    it('renders without errors', () => {
        const { getByLabelText } = render(<LoginForm />);
        const loginEmailInput = getByLabelText('Email');
        expect(loginEmailInput).toBeInTheDocument();
    });

    it('handles form submission correctly', () => {
        window.alert = jest.fn();
        const { getByText, getByLabelText } = render(<LoginForm />);
        const emailInput = getByLabelText('Email');
        const passwordInput = getByLabelText('Password');
        const submitButton = getByText('Login');

        fireEvent.change(emailInput, { target: { value: 'example@example.com' } });
        fireEvent.change(passwordInput, { target: { value: '12345' } });
        fireEvent.click(submitButton);
        window.alert.mockClear();
    });

    it('handles form submission with error', () => {
        window.alert = jest.fn();
        const { getByText, getByLabelText } = render(<LoginForm />);
        const emailInput = getByLabelText('Email');
        const passwordInput = getByLabelText('Password');
        const submitButton = getByText('Login');

        fireEvent.change(emailInput, { target: { value: 'test' } });
        fireEvent.change(passwordInput, { target: { value: '12345' } });
        fireEvent.click(submitButton);
        window.alert.mockClear();
        const emailErrorText = getByText('You have to provide a valid email.');
        expect(emailErrorText).toBeInTheDocument();
    });
});





