import React from "react";
import Form from 'react-bootstrap/Form';

export default function Input({ id, name, type, placeholder='', value, label, onChange, error, required= false }) {
    return (
        <div className={`form-item ${error ? 'has-error' : ''}`}>
            <Form.Group className="mb-3">
                <Form.Label htmlFor={ id }>{ label }</Form.Label>
                <Form.Control required={required} name={ name } id={ id } type={ type } placeholder= { placeholder } value={ value } onChange={(e) => onChange(e, id)}/>
            </Form.Group>
            {error && <span className="error-message">{error}</span>}
        </div>
    );
}
