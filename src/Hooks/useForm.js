import {useEffect, useState} from 'react'
import {omit} from 'lodash'

const useForm = (callback, formFields) => {

    //Form values
    const [values, setValues] = useState(formFields);
    //Errors
    const [errors, setErrors] = useState({});

    useEffect(() => {
        validateMatchConfirmPassword()
    }, [values]);

    const validate = (name, value) => {
        //A function to validate each input values
        switch (name) {
            case 'email':
                if (!value?.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/)) {
                    setErrors({
                        ...errors,
                        email: ['You have to provide a valid email.'],
                    });
                } else {
                    setErrors(omit(errors, 'email'));
                }
                break;

            case 'password': case 'confirm':
                validatePassword(name, value);
                break;

            default:
                break;
        }
    }

    const validatePassword = (name, value) => {
        if (value?.length < 5 || value?.length > 16) {
            setErrors({
                ...errors,
                [name]: ['Your password must be between 5 and 16 characters length.'],
            });
        } else {
            setErrors(omit(errors, name));
        }
    }

    const validateMatchConfirmPassword = () => {
        if (values.password?.length > 0  && values.confirm?.length > 0 && values.confirm !== values.password) {
            setErrors({
                ...errors,
                confirm: ['Your passwords must match.'],
            });
        } else {
            setErrors(omit(errors, 'confirm'));
        }
    }

    //A method to handle form inputs
    const handleChange = (event) => {

        event.persist();

        let name = event.target.name;
        let val = event.target.value;

        setValues({
            ...values,
            [name]:val,
        })


        validate(name,val);
    }

    const handleSubmit = (event) => {
        if(event) event.preventDefault();
        if(Object.keys(errors).length === 0 && Object.keys(values).length !==0 ){
            callback();
        }
    }

    return {
        values,
        errors,
        handleChange,
        handleSubmit
    }
}

export default useForm
