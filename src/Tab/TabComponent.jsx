import React, { Component } from 'react';
import RegistrationForm from "../Form/RegistrationForm";
import LoginForm from "../Form/LoginForm";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import {Card} from "react-bootstrap";

class TabComponent extends Component {
    render() {
        return (
            <Card  style={{ width: '40rem' }}>
                <Card.Body>
                    <Card.Title className="text-center">Test FuelMarket</Card.Title>
                    <Tabs
                        defaultActiveKey="Login"
                        id="login_tab"
                        className="justify-content-center"
                    >
                        <Tab eventKey="Login" title="J'ai un compte">
                            <LoginForm />
                        </Tab>
                        <Tab eventKey="Register" title="Je n'ai pas de compte">
                            <RegistrationForm />
                        </Tab>
                    </Tabs>
                </Card.Body>
            </Card>
        );
    }
}

export default TabComponent;
